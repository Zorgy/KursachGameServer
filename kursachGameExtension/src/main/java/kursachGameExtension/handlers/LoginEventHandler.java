package kursachGameExtension.handlers;

import com.smartfoxserver.bitswarm.sessions.ISession;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.exceptions.SFSErrorCode;
import com.smartfoxserver.v2.exceptions.SFSErrorData;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.exceptions.SFSLoginException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;
import com.smartfoxserver.v2.security.DefaultPermissionProfile;

import java.sql.SQLException;

public class LoginEventHandler extends BaseServerEventHandler {
    public void handleServerEvent(ISFSEvent event) throws SFSException {

        String name = (String) event.getParameter(SFSEventParam.LOGIN_NAME);
        String password = (String) event.getParameter(SFSEventParam.LOGIN_PASSWORD);
        ISession isession = (ISession) event.getParameter(SFSEventParam.SESSION);

        if (name.equals("Guest"))
        {
            isession.setProperty("$permission", DefaultPermissionProfile.GUEST);
            trace("Guest login");
            return;
        }

        IDBManager dbm = getParentExtension().getParentZone().getDBManager();
        String sql = "SELECT Password FROM Users  WHERE Username=?";

        try {
            ISFSArray result = dbm.executeQuery(sql, new Object[]{name});

            if (result.size() == 0) {
                SFSErrorData errData = new SFSErrorData(SFSErrorCode.LOGIN_BAD_USERNAME);
                errData.addParameter(name);
                throw new SFSLoginException("Incorrect username", errData);
            }

            if (!getApi().checkSecurePassword(isession, result.getSFSObject(0).getUtfString("Password"), password)) {
                SFSErrorData errData2 = new SFSErrorData(SFSErrorCode.LOGIN_BAD_PASSWORD);
                errData2.addParameter(password);
                throw new SFSLoginException("Incorrect password", errData2);
            }

            isession.setProperty("$permission", DefaultPermissionProfile.STANDARD);
            
        } catch (SQLException e) {
            trace(ExtensionLogLevel.WARN, new Object[]{"SQL Failed: " + e.toString()});
        }
    }
}