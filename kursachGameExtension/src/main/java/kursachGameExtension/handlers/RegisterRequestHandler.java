package kursachGameExtension.handlers;

import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

public class RegisterRequestHandler extends BaseClientRequestHandler
{
    @Override
    public void handleClientRequest(User u, ISFSObject data)
    {
        String login = data.getUtfString("login");
        String password = data.getUtfString("password");

        ISFSObject objOut = new SFSObject();

        
        try {
            IDBManager dbm = getParentExtension().getParentZone().getDBManager();

            String sql1 = "SELECT Username FROM Users WHERE Username=?";
            ISFSArray res = dbm.executeQuery(sql1, new Object[]{login});
            
            if (res.size() > 0)
            {
                objOut.putBool("success", false);
                objOut.putUtfString("reason", "Пользователь уже существует");
                send("register_user", objOut, u);
                return;
            }

            String sql2 = "INSERT INTO Users (Username, Password) VALUES(?, ?);";
            dbm.executeInsert(sql2, new Object[]{login, password});
            
        } catch (SQLException e) {
            trace(ExtensionLogLevel.WARN, new Object[]{"SQL Failed: " + e.toString()});

            objOut.putBool("success", false);
            objOut.putUtfString("reason", e.toString());
            send("register_user", objOut, u);
        }

        objOut.putBool("success", true);
        objOut.putUtfString("reason", "Успешно");
        send("register_user", objOut, u);
        
    }
}