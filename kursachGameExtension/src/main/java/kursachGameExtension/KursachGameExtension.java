package kursachGameExtension;

import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.extensions.SFSExtension;
import kursachGameExtension.handlers.*;

public class KursachGameExtension extends SFSExtension
{
    @Override
    public void init()
    {
        trace("Kursach extension start");

        addRequestHandler("register_user", RegisterRequestHandler.class);

        addEventHandler(SFSEventType.USER_LOGIN, LoginEventHandler.class);
    }
}
