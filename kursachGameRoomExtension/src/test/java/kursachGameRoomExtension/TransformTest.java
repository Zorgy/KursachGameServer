package kursachGameRoomExtension;

import org.junit.Test;

import kursachGameRoomExtension.types.Transform;

import static org.junit.Assert.*;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;


public class TransformTest {

    Transform transform = new Transform(1, 2, 3, 4, 5, 6);

    @Test 
    public void testConstructor2() {
        Transform transform2 = new Transform(1,2,3);

        assertTrue("Error in constructor 2",
            transform2.x == 1 &&
            transform2.y == 2 &&
            transform2.z == 3 &&
            transform2.rotx == 0 &&
            transform2.roty == 0 &&
            transform2.rotz == 0
        );
    }

    @Test 
    public void testConstructor3() {
        Transform test_transform = new Transform(transform);

        assertTrue("Error in constructor 3",
            test_transform.x == transform.x &&
            test_transform.y == transform.y &&
            test_transform.z == transform.z &&
            test_transform.rotx == transform.rotx &&
            test_transform.roty == transform.roty &&
            test_transform.rotz == transform.rotz
        );
    }

    @Test 
    public void testToSFSObject() {

        ISFSObject data = new SFSObject();

        transform.toSFSObject(data);

        ISFSObject TransformObj = data.getSFSObject("transform");

        assertTrue("Error in to SFS",
            TransformObj.getDouble("x") == 1 &&
            TransformObj.getDouble("y") == 2 &&
            TransformObj.getDouble("z") == 3 &&
            TransformObj.getDouble("rx") == 4 &&
            TransformObj.getDouble("ry") == 5 &&
            TransformObj.getDouble("rz") == 6
        
        );
    }

    @Test 
    public void testFromSFSObject() {

        ISFSObject TransformObj = new SFSObject();

        TransformObj.putDouble("x", 1);
        TransformObj.putDouble("y", 2);
        TransformObj.putDouble("z", 3);
        TransformObj.putDouble("rx", 4);
        TransformObj.putDouble("ry", 5);
        TransformObj.putDouble("rz", 6);

        ISFSObject data = new SFSObject();
        data.putSFSObject("transform", TransformObj);

        Transform tr = Transform.fromSFSObject(data);

        assertTrue("Error in From SFS",
            tr.x == 1 &&
            tr.y == 2 &&
            tr.z == 3 &&
            tr.rotx == 4 &&
            tr.roty == 5 &&
            tr.rotz == 6
        );
    }
}