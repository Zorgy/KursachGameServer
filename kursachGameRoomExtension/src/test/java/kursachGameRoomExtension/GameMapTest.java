package kursachGameRoomExtension;

import org.junit.Test;

import kursachGameRoomExtension.controllers.Game;
import kursachGameRoomExtension.controllers.GameMap;

import static org.junit.Assert.*;

public class GameMapTest {

    GameMap gm = new GameMap("outpost on desert");

    @Test(expected = IllegalArgumentException.class)
    public void testWithIncorrectMap() {
        GameMap map = new GameMap("asdddd"); 
    }

    @Test
    public void testGetSpawnPoint() {
        gm.GetSpawnPoint();
        gm.GetSpawnPoint();

        assertNull("Expected only 2 spawnpoints", gm.GetSpawnPoint());
    }
}
