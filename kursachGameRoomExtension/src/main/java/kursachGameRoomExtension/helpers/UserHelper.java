package kursachGameRoomExtension.helpers;

import java.util.List;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;

public class UserHelper {
    public static List<User> getUsers(Room room)
    {
        return room.getUserList();
    }
}
