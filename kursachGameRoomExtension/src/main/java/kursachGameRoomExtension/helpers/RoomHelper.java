package kursachGameRoomExtension.helpers;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
import com.smartfoxserver.v2.extensions.SFSExtension;

import kursachGameRoomExtension.KursachGameRoomExtension;
import kursachGameRoomExtension.controllers.Game;

public class RoomHelper {

    public static Room getRoom(BaseClientRequestHandler handler)
    {
        return handler.getParentExtension().getParentRoom();
    }

    public static Room getRoom(SFSExtension ext)
    {
        return ext.getParentRoom();
    }

    public static Game getGame(BaseClientRequestHandler handler)
    {
        KursachGameRoomExtension ext = (KursachGameRoomExtension) handler.getParentExtension();
        return ext.game;
    }

    public static Game getGame(BaseServerEventHandler handler)
    {
        KursachGameRoomExtension ext = (KursachGameRoomExtension) handler.getParentExtension();
        return ext.game;
    }
}
