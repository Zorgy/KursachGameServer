package kursachGameRoomExtension;

import java.util.Date;
import java.util.List;

import javax.xml.crypto.dsig.Transform;

import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.SFSExtension;

import kursachGameRoomExtension.controllers.Game;
import kursachGameRoomExtension.controllers.GameUser;
import kursachGameRoomExtension.handlers.*;
import kursachGameRoomExtension.helpers.RoomHelper;
import kursachGameRoomExtension.helpers.UserHelper;

public class KursachGameRoomExtension extends SFSExtension
{
    public Game game;

    @Override
    public void init()
    {
        game = new Game(this);
        trace("Room Extension start");

        addRequestHandler("get_time", TimeRequest.class);
        addRequestHandler("spawn_me", SpawnPlayerRequest.class);
        addRequestHandler("send_transform", SendTransformRequest.class);
        addRequestHandler("send_animation", SendAnimationRequest.class);
        addRequestHandler("change_weapon", ChangeWeaponRequest.class);
        addRequestHandler("bullet_shot", BulletShotRequest.class);

        addEventHandler(SFSEventType.ROOM_VARIABLES_UPDATE, RoomVarEventHandler.class);
    }

    public void KillUser(GameUser user)
    {
        Room currRoom = RoomHelper.getRoom(this);
        List<User> userList = UserHelper.getUsers(currRoom);
        
        ISFSObject data = new SFSObject();
        user.toSFSObject(data);

        this.send("kill_user", data, userList);
    }

    public void clientChangeTransform(ISFSObject transformObj)
    {
        Room currRoom = RoomHelper.getRoom(this);
        List<User> userList = UserHelper.getUsers(currRoom);
        transformObj.putLong("timestamp", new Date().getTime());

        this.send("send_transform", transformObj, userList, true);
    }

    public void clientInstantiatePlayer(GameUser player, User targetUser)
    {
        ISFSObject data = new SFSObject();

        player.toSFSObject(data);
        Room currRoom = RoomHelper.getRoom(this);

        data.putLong("timestamp", new Date().getTime());
        if (targetUser == null)
        {  
            List<User> userList = UserHelper.getUsers(currRoom);
            this.send("spawn_player", data, userList);
        }
        else
        {
            this.send("spawn_player", data, targetUser);
        }
    }
}
