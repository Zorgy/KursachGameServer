package kursachGameRoomExtension.handlers;

import java.util.Date;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class TimeRequest extends BaseClientRequestHandler
{
    @Override
    public void handleClientRequest(User u, ISFSObject data)
    {
        ISFSObject res = new SFSObject();
        Date date = new Date();
        res.putLong("time", date.getTime());
        this.send("time", res, u);
    }
}
