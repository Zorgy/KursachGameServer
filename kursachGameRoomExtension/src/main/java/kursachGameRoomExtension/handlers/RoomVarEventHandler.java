package kursachGameRoomExtension.handlers;

import java.util.List;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

import kursachGameRoomExtension.controllers.Game;
import kursachGameRoomExtension.helpers.RoomHelper;

public class RoomVarEventHandler extends BaseServerEventHandler{

    @Override
    public void handleServerEvent(ISFSEvent event) throws SFSException {

        Game game = RoomHelper.getGame(this);

        Room room = (Room) event.getParameter(SFSEventParam.ROOM);
        List<RoomVariable> RoomVars = (List<RoomVariable>) event.getParameter(SFSEventParam.VARIABLES);

        for (RoomVariable roomVariable : RoomVars) {
            if (roomVariable.getName().equals("GameStarted"))
            {
                game.SetMap((String) room.getVariable("RoomMap").getValue());
                trace("Game started with map: " + (String) room.getVariable("RoomMap").getValue());
            }
        }
    }
    
}
