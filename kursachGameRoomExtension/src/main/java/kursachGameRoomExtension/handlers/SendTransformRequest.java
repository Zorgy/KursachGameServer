package kursachGameRoomExtension.handlers;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

import org.apache.commons.lang.NullArgumentException;

import kursachGameRoomExtension.KursachGameRoomExtension;
import kursachGameRoomExtension.controllers.Game;
import kursachGameRoomExtension.controllers.GameUser;
import kursachGameRoomExtension.helpers.RoomHelper;
import kursachGameRoomExtension.types.Transform;

public class SendTransformRequest extends BaseClientRequestHandler
    {
        @Override
        public void handleClientRequest(User u, ISFSObject data) throws NullArgumentException
        {
            Game game = RoomHelper.getGame(this);
            KursachGameRoomExtension ext = (KursachGameRoomExtension)this.getParentExtension();

            Transform transform = Transform.fromSFSObject(data);

            if (transform != null)
            {
                GameUser gameUser = game.GetPlayer(u.getId());

                gameUser.CurPos = transform;

                ISFSObject transformObj = new SFSObject();

                transformObj.putInt("id", u.getId());
                gameUser.CurPos.toSFSObject(transformObj);

                ext.clientChangeTransform(transformObj);
            }
            else
            {
                throw new NullArgumentException("Transform is null");
            }
        }
    }