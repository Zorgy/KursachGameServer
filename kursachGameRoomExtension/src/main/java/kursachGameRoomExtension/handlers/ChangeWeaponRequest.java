package kursachGameRoomExtension.handlers;

import java.util.Date;
import java.util.List;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

import kursachGameRoomExtension.controllers.Game;
import kursachGameRoomExtension.controllers.GameUser;
import kursachGameRoomExtension.helpers.RoomHelper;
import kursachGameRoomExtension.helpers.UserHelper;

public class ChangeWeaponRequest extends BaseClientRequestHandler {

    @Override
    public void handleClientRequest(User u, ISFSObject data)
    {
        Game game = RoomHelper.getGame(this);

        String weapon = data.getUtfString("weapon");
        GameUser player = game.GetPlayer(u.getId());

        player.SetWeapon(weapon);

        Room currRoom = RoomHelper.getRoom(this);
        List<User> userList = UserHelper.getUsers(currRoom);

        ISFSObject weaponObj = new SFSObject();

        weaponObj.putInt("id", u.getId());
        weaponObj.putUtfString("weapon", weapon);
        weaponObj.putLong("timestamp", new Date().getTime());

        send("change_weapon", weaponObj, userList);
    }
}
