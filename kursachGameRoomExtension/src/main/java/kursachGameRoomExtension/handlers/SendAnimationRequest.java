package kursachGameRoomExtension.handlers;

import java.util.Date;
import java.util.List;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

import kursachGameRoomExtension.helpers.RoomHelper;
import kursachGameRoomExtension.helpers.UserHelper;

public class SendAnimationRequest extends BaseClientRequestHandler {

    @Override
    public void handleClientRequest(User u, ISFSObject data)
    {
        String animState = data.getUtfString("animation");
        ISFSObject animObj = new SFSObject();

        animObj.putUtfString("animation", animState);
        animObj.putInt("id", u.getId());
        animObj.putLong("timestamp", new Date().getTime());

        Room currRoom = RoomHelper.getRoom(this);
        List<User> userList = UserHelper.getUsers(currRoom);

        send("send_animation", animObj, userList);
    }
}
