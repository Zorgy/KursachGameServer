package kursachGameRoomExtension.handlers;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

import kursachGameRoomExtension.controllers.Game;
import kursachGameRoomExtension.helpers.RoomHelper;

public class BulletShotRequest extends BaseClientRequestHandler {

    @Override
    public void handleClientRequest(User u, ISFSObject data)
    {
        Game game = RoomHelper.getGame(this);

        Integer id = data.getInt("id");
        trace(id);
        
        game.Damageplayer(id, 20);

        ISFSObject req = new SFSObject();
        req.putInt("HP", game.GetPlayer(id).HP);

        send("update_hp", req, game.GetPlayer(id).sfsUser);
    }
}
