package kursachGameRoomExtension.handlers;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

import kursachGameRoomExtension.controllers.Game;
import kursachGameRoomExtension.helpers.RoomHelper;

public class SpawnPlayerRequest extends BaseClientRequestHandler
{
    @Override
    public void handleClientRequest(User u, ISFSObject data)
    {
        trace("Spawning user");
        Game game = RoomHelper.getGame(this);

        game.SpawnPlayer(u);
    }
}
