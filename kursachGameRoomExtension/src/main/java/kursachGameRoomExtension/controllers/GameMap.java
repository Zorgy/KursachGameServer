package kursachGameRoomExtension.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.smartfoxserver.v2.extensions.SFSExtension;

import kursachGameRoomExtension.KursachGameRoomExtension;
import kursachGameRoomExtension.types.Transform;

public class GameMap {

    public final String[] MapArray = new String[] 
    {
        "outpost on desert"
    };

    public String name;
    public List<Transform> SpawnPoints = new ArrayList<Transform>();

    private final Random rand;

    public GameMap(String name) throws IllegalArgumentException
    {
        if (Arrays.asList(MapArray).contains(name))
        {
            this.name = name;
            genTransforms();
            
            rand = new Random(System.currentTimeMillis());
        }
        else 
        {
            throw new IllegalArgumentException("Invalid map name: " + name);
        }
    }

    public Transform GetSpawnPoint()
    {   
        if (SpawnPoints.size() > 0)
        {

            int index = rand.nextInt(SpawnPoints.size());
            Transform transform = SpawnPoints.get(index);
            SpawnPoints.remove(index);

            return transform;
        }
        
        return null;
    }

    private void genTransforms()
    {
        if (name.equals("outpost on desert"))
        {
            SpawnPoints.add(new Transform(0, 1.722, -4.887));
            SpawnPoints.add(new Transform(-4.603, 1.722, 0.083));
        }
        
    }

}
