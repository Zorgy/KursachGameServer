package kursachGameRoomExtension.controllers;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

import kursachGameRoomExtension.types.Weapons;
import kursachGameRoomExtension.types.Transform;

public class GameUser {

    public User sfsUser;
    public Transform CurPos;
    
    public int HP = 100;
    private Weapons Weapon;

    public GameUser(User user)
    {
        this.sfsUser = user;
    }

    public boolean IsDead()
    {
        return HP<=0;
    }

    public void ChangeWeapon(String WeaponName)
    {
        if (WeaponName == "None")
        {
            Weapon = Weapons.None;
        }
        else if (WeaponName == "Rifle")
        {
            Weapon = Weapons.Rifle;
        }
    }

    public Weapons GetWeapon()
    {
        return Weapon;
    }

    public void SetWeapon(String Weapon)
    {
        if (Weapon.equals("Rifle"))
        {
            this.Weapon = Weapons.Rifle;
        }
        else if (Weapon.equals("None"))
        {
            this.Weapon = Weapons.None;
        }
    }

    public void toSFSObject(ISFSObject obj)
    {
        ISFSObject player = new SFSObject();

        player.putInt("id", sfsUser.getId());

        CurPos.toSFSObject(player);
        
        obj.putSFSObject("player", player);
    }
}
