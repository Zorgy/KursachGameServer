package kursachGameRoomExtension.controllers;

import java.util.HashMap;
import java.util.Map;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

import kursachGameRoomExtension.KursachGameRoomExtension;

public class Game {
    
    private Map<Integer, GameUser> Users = new HashMap<Integer, GameUser>();
    private GameMap map;
    private KursachGameRoomExtension ext;

    public Game(KursachGameRoomExtension ext)
    {
        this.ext = ext;
    }

    public void SetMap(String mapName)
    {
        this.map = new GameMap(mapName);
    }

    public void SpawnPlayer(User u)
    {
        GameUser player = new GameUser(u);
        player.CurPos = map.GetSpawnPoint();

        Users.put(u.getId(), player);

        ext.clientInstantiatePlayer(player, null);
    }

    public GameUser GetPlayer(int id)
    {
        return Users.get(id);
    }

    public void Damageplayer(int id, int count)
    {
        Users.get(id).HP -= count;
        if (Users.get(id).IsDead())
        {
            ext.KillUser(Users.get(id));
        }
    }
}
