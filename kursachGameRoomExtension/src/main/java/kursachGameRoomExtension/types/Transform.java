package kursachGameRoomExtension.types;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Transform {
    public double x;
    public double y;
    public double z;

    public double rotx = 0;
    public double roty = 0;
    public double rotz = 0;

    public Transform(double x, double y, double z, double rotx, double roty, double rotz)
    {
        this.x = x;
        this.y = y;
        this.z = z;

        this.rotx = rotx;
        this.roty = roty;
        this.rotz = rotz;
    }

    public Transform(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Transform(Transform another)
    {
        this.x = another.x;
        this.y = another.y;
        this.z = another.z;

        this.rotx = another.rotx;
        this.roty = another.roty;
        this.rotz = another.rotz;
    }

    public static Transform fromSFSObject(ISFSObject obj)
    {
        ISFSObject TransformObj = obj.getSFSObject("transform");
        double x = TransformObj.getDouble("x");
        double y = TransformObj.getDouble("y");
        double z = TransformObj.getDouble("z");

        double rotx = TransformObj.getDouble("rx");
        double roty = TransformObj.getDouble("ry");
        double rotz = TransformObj.getDouble("rz");

        Transform transform = new Transform(x, y, z, rotx, roty, rotz);
        return transform;
    }

    public void toSFSObject(ISFSObject obj)
    {
        ISFSObject TransformObj = new SFSObject();

        TransformObj.putDouble("x", x);
        TransformObj.putDouble("y", y);
        TransformObj.putDouble("z", z);

        TransformObj.putDouble("rx", rotx);
        TransformObj.putDouble("ry", roty);
        TransformObj.putDouble("rz", rotz);

        obj.putSFSObject("transform", TransformObj);
    }

}
