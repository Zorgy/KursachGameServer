CREATE DATABASE IF NOT EXISTS Kursach_game;

use Kursach_game

CREATE TABLE IF NOT EXISTS Users 
(
  UserID INT AUTO_INCREMENT,
  Username VARCHAR(25) NOT NULL,
  Password VARCHAR(32) NOT NULL,
  PRIMARY KEY(UserID)
);

CREATE USER IF NOT EXISTS 'Kursach_game_admin'@'%' identified by 'kursach';

GRANT ALL PRIVILEGES ON Kursach_game.* to 'Kursach_game_admin'@'%' WITH GRANT OPTION;