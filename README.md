# KursachGameServer
## Зависимости
* jdk-8 для сборки расширений сервера;
* jre-8 или выше для запуска сервера;

## Сборка расширений
Расширения находятся в директориях kursachGameExtension и kursachGameRoomExtension. Для сборки необходимо перейти в директорию с расширением и выполнить ./gradlew build, после этого в build/libs будет создан .jar файл с расширением.

## Развертывание сервера
1.  распаковать архив «SFS2X_unix_2_14_0.tar.gz»;
2.  скопировать директории config, extension, zones из ServerDumpForImportв директорию SmartFoxServer_2X/SFS2X/;
3.  скопировать файлы из ServerDumpForImport/Deps в в директориюSmartFoxServer_2X/SFS2X/lib;
4.  выполнитьSQLзапросыизфайлаdatabase/init­db.sql,длясозданияновойбазы данных и таблицы;
5.  запустить sfs2x.sh.

## Развертывание с использованием докера\
1.  скопировать директории config, zones из ServerDumpForImport в дирек­торию volumes/server;
2.  выполнить команду: docker­compose up –build -­d.