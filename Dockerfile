FROM openjdk:8-jre

COPY SFS2X_unix_2_14_0.tar.gz .
RUN tar -xf SFS2X_unix_2_14_0.tar.gz

WORKDIR /SmartFoxServer_2X/SFS2X

COPY ServerDumpForImport/extensions/ extensions/
COPY ServerDumpForImport/Deps/ lib/
COPY ServerDumpForImport/wait-for-it.sh/ .

ENTRYPOINT [ "./wait-for-it.sh", "database:3306", "--", "./sfs2x.sh" ] 